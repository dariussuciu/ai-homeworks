import sys
import math
# global variables
try:
    INFINITY = float("infinity")
    NEG_INFINITY = float("-infinity")
except ValueError:                 # Windows doesn't support 'float("infinity")'.
    INFINITY = float(1e3000)       # However, '1e3000' will overflow and return
    NEG_INFINITY = float(-1e3000)  # the magic float Infinity value anyway.
# global variables
category_dict = {}	
ranges_dict = {}
continuous = [1, 2, 7, 10, 13, 14]
attribute_error_stats = {}

def parse_training_set(filepath):
	global category_dict
	global ranges_dict
	# inefficient, but conceptually simple - several passes through the data to 
	# deal with continuous values and missing values
	# first pass through the training data: get ranges for the continuous values
	for i in range(0, 15):
		ranges_dict[i] = {}
	for i in continuous:
		ranges_dict[i]['max'] = NEG_INFINITY	
		ranges_dict[i]['min'] = INFINITY 	
	fh = open(filepath)
	for row in fh.readlines():
		line = row.strip().split(',')
		for i in range(len(line)):
			if i in continuous and line[i] != '?':
				if ranges_dict[i]['max'] < float(line[i]):
					ranges_dict[i]['max'] = float(line[i])
				if ranges_dict[i]['min'] > float(line[i]):
					ranges_dict[i]['min'] = float(line[i])
	fh.close()
	for i in continuous:
		ranges_dict[i]['interval'] = (ranges_dict[i]['max'] - ranges_dict[i]['min'])/10
	# second pass through the training data: figure out the most common category for each attribute 
	# also do the check to make sure there are no errors in the input
	for i in continuous:
		category_dict[i] = {}
		for j in range(0, 11):
			category_dict[i][j] = 0
	category_dict[0] = {}	
	for j in ['a', 'b']:
		category_dict[0][j] = 0
	category_dict[3] = {}	
	for j in ['u', 'y', 'l', 't']:
		category_dict[3][j] = 0
	category_dict[4] = {}	
	for j in ['g', 'p', 'gg']:
		category_dict[4][j] = 0
	category_dict[5] = {}	
	for j in ['c', 'd', 'cc', 'i', 'j', 'k', 'm', 'r', 'q', 'w', 'x', 'e', 'aa', 'ff']:
		category_dict[5][j] = 0
	category_dict[6] = {}	
	for j in ['v', 'h', 'bb', 'j', 'n', 'z', 'dd', 'ff', 'o']:
		category_dict[6][j] = 0
	category_dict[8] = {}	
	for j in ['t', 'f']:
		category_dict[8][j] = 0
	category_dict[9] = {}	
	for j in ['t', 'f']:
		category_dict[9][j] = 0
	category_dict[11] = {}	
	for j in ['t', 'f']:
		category_dict[11][j] = 0
	category_dict[12] = {}	
	for j in ['g', 'p', 's']:
		category_dict[12][j] = 0
	fh = open(filepath)
	for row in fh.readlines():
		line = row.strip().split(',')
		for i in range(len(line) - 1): # ignoring the classification attribute 
			if line[i] != '?':
				category = line[i]
				if i in continuous:
					if float(line[i]) < ranges_dict[i]['min']:
						category = 0	
					elif float(line[i]) > ranges_dict[i]['max']:
						category = 9
					else:
						category = int((float(line[i]) - ranges_dict[i]['min'])/ranges_dict[i]['interval'])
				if (category not in category_dict[i]):
					print "error with line"
					print line
				category_dict[i][category] = category_dict[i][category] + 1
	fh.close()
	for i in range(0, 15):
		v=list(category_dict[i].values())
		k=list(category_dict[i].keys())
		most_common = k[v.index(max(v))]
		ranges_dict[i]['most_common'] = most_common
	# final pass through the training data: assign categories for continuous values and fill in missing values with the most common category for that attribute
	fh = open(filepath)
	data = [row.strip().split(',') for row in fh.readlines()]
	for i in range(0, len(data)):
		for j in range(0, 15):
			# fill in the missing value
			if data[i][j] == '?':
				data[i][j] = ranges_dict[j]['most_common']
			if j in continuous:
				if float(data[i][j]) < ranges_dict[j]['min']:
					data[i][j] = 0	
				elif float(data[i][j]) > ranges_dict[j]['max']:
					data[i][j] = 9
				else:
					data[i][j] = int((float(data[i][j]) - ranges_dict[j]['min'])/ranges_dict[j]['interval'])
	#print data
	#print category_dict
	fh.close()
	return data

def calculate_entropy(data):
	# entropy is the sum of (probabilities times log_2 of probabilities)
	# we do this relative to the classification of +/-;  add 1 to avoid log of 0
	num_plus = 0.0
	num_minus = 0.0
	#print data
	for i in range(0, len(data)):
		if data[i][15] == '+':
			num_plus = num_plus + 1.0
		else:
			num_minus = num_minus + 1.0
	if num_plus == 0.0 or num_minus == 0.0:
		return 0
	entropy = (-1 *num_plus/len(data)) * math.log(num_plus/len(data), 2) 
	entropy = entropy + (-1 * num_minus/len(data)) * math.log(num_minus/len(data), 2) 
	return entropy

def calculate_information_gain(data, test_attribute):
	#IG(Y|X) = H(Y) - H(Y|X)
	global category_dict	

	# how many times does each value of the test_attribute appears
	for k in category_dict[test_attribute].keys():
		category_dict[test_attribute][k] = 0.0
	for i in range(0, len(data)):
		category_dict[test_attribute][data[i][test_attribute]] += 1.0

	# for each value of the test attribute, get the specific conditional entropy, multiply by the freqeuncy of this value
	# and add to the running sum of the average conditional entropy
	average_conditional_entropy = 0.0
	for k in category_dict[test_attribute].keys():
		#if k == 'most_common': 	
		#	continue
		if not category_dict[test_attribute][k]: # empty
			continue
		# get all entries that have value k for test_attribute
		data_with_value = []
		for i in range(0, len(data)):
			if data[i][test_attribute] == k:
				tmp = data[i]
				tmp2 = tmp[:]
				data_with_value.append(tmp2)
		#print "calling calculate entropy"
		#print "test attribute"
		#print test_attribute
		#print "value"
		#print k 
		specific_conditional_entropy = calculate_entropy(data_with_value)
		value_probability = category_dict[test_attribute][k] / len(data)
		average_conditional_entropy += value_probability * specific_conditional_entropy
		
	information_gain = calculate_entropy(data) - average_conditional_entropy 
	return information_gain

def pick_next_split_attribute(data):
	# select the attribute with the largest information gain
	max_information_gain = 0
	pick_attribute = -1 
	for i in range(0, 15):
		#print "calling calculate_information_gain"
		information_gain = calculate_information_gain(data, i)
		if information_gain > max_information_gain:
			max_information_gain = information_gain
			pick_attribute = i
	return pick_attribute 

def generate_decision_tree(training_data, attributes, stats_name):
	global attribute_error_stats
	# data structure for later pruning
	attribute_error_stats[stats_name] = {}	
	attribute_error_stats[stats_name]['num_plus'] = 0
	attribute_error_stats[stats_name]['num_minus'] = 0
	#attribute_error_stats[stats_name]['num_misclassified'] = 0
	# based on our class distribution, we should return '-' classification when we have no other information
	if attributes == set() or not training_data:
		return "-"  # leaf node of the decision tree - final classification:  TODO: fix this to get majority of the actual sample
	# check if we can stop - this subset of training data is all "+" or all "-"
	num_plus = 0
	num_minus = 0
	for i in range(0, len(training_data)):
		if training_data[i][15] == '+':
			num_plus = num_plus + 1
		else:
			num_minus = num_minus + 1
	if num_plus == len(training_data):
		return '+'  # leaf node of the decision tree, final classification
	if num_minus == len(training_data):
		return '-'  # leaf node of the decision tree, final classification
	# we need to split further
	# pick the next split attribute
	sa = pick_next_split_attribute(training_data)
	if sa == -1:  # this can happen if by filling in the missing values, we accidentally create two identical records with different classification
		return "-"
	#sa = next(iter(attributes)) # we simply pick the next sequential attribute, before figuring out the optimal one based on information gain 
	# the decision tree will be a multi-level dictionary, where each level is the attribute split point
	decision_tree = {sa:{}}
	for value in category_dict[sa].keys():
		#if value == 'most_common': 	
		#	continue
		#print value	
		# get subset of remaining training data for this attribute value
		subtree_training_data = []
		for i in range(0, len(training_data)):
			if training_data[i][sa] == value:
				tmp = training_data[i]
				tmp2 = tmp[:]
				subtree_training_data.append(tmp2)
				#print tmp2
		subtree_attributes = set(attributes)
		subtree_attributes.remove(sa)
		new_stats_name = stats_name + "_" + str(sa) + "_" + str(value)
		subtree =  generate_decision_tree(subtree_training_data, subtree_attributes, new_stats_name)
		#print subtree
		decision_tree[sa][value] = subtree
	return decision_tree

def perform_pruning(decision_tree, infilepath):
	in_fh = open(infilepath)
	data = [row.strip().split(',') for row in in_fh.readlines()]
	# do a pass through the validation set, and assign data to each node in the decision tree 
	for i in range(0, len(data)):
		# fill in the missing values and discretize the continuous variables
		for j in range(0, 15):
			if data[i][j] == '?':
				data[i][j] = ranges_dict[j]['most_common']
			if j in continuous:
				if float(data[i][j]) < ranges_dict[j]['min']:
					data[i][j] = 0	
				elif float(data[i][j]) > ranges_dict[j]['max']:
					data[i][j] = 9
				else:
					data[i][j] = int((float(data[i][j]) - ranges_dict[j]['min'])/ranges_dict[j]['interval'])
		v_tree = decision_tree
		stats_name = "TOP"
		# iterate through the attribute and value levels of the decision tree until reaching the classification 
		while v_tree != '+' and v_tree != '-':
			# update the plus and minus
			if data[i][15] == '+':
				attribute_error_stats[stats_name]['num_plus'] += 1
			else:
				attribute_error_stats[stats_name]['num_minus'] += 1
			# keep classifying
			attribute = v_tree.keys()[0] 
			a_tree = v_tree[attribute]
			data_attribute_value = data[i][attribute]
			stats_name = stats_name + "_" + str(attribute) + "_" + str(data_attribute_value)
			#print stats_name
			if data_attribute_value in a_tree:
				v_tree = a_tree[data_attribute_value]
				#print v_tree
			else: # default value
				v_tree = '-'
				break	
		# update the plus and minus
		if data[i][15] == '+':
			attribute_error_stats[stats_name]['num_plus'] += 1
		else:
			attribute_error_stats[stats_name]['num_minus'] += 1

	# call a recursive function that will remove sub-trees if their error rate is higher than the parent
	# it mirrors the form of generate decision tree, but in this case, cuts off the subtrees instead of adding them 
	#print decision_tree
	#print attribute_error_stats
	decision_tree, error_estimate, num_samples = remove_subtrees(decision_tree, "TOP")
	return decision_tree

def remove_subtrees(v_tree, stats_name):
	#print stats_name
	# return the number_mis-classified
	node_N = attribute_error_stats[stats_name]['num_minus'] + attribute_error_stats[stats_name]['num_plus']
	z = 0.69 # from normal distribution, equal to confidence level of 75%
	node_f = 0
	node_error_estimate = 0
	if node_N != 0:
		node_f = min(attribute_error_stats[stats_name]['num_minus'],attribute_error_stats[stats_name]['num_plus'])/node_N
		node_error_estimate = (node_f + (z*z)/2*node_N + z*math.sqrt(node_f/node_N - node_f*node_f/node_N + z*z/4*node_N*node_N))/(1+z*z/node_N)
	if v_tree == '+':
		return '+', node_error_estimate, node_N 
	if v_tree == '-':
		return '-', node_error_estimate, node_N 
	attribute = v_tree.keys()[0] 
	a_tree = v_tree[attribute]
	total_sub_tree_error_estimate = 0
	decision_tree = {attribute:{}}
	for data_attribute_value in a_tree.keys():
		sub_v_tree = a_tree[data_attribute_value]
		sub_stats_name = stats_name + "_" + str(attribute) + "_" + str(data_attribute_value)
		subtree, sub_tree_error_estimate, subtree_N = remove_subtrees(sub_v_tree, sub_stats_name)
		if node_N != 0:
			total_sub_tree_error_estimate += sub_tree_error_estimate*subtree_N/node_N
		decision_tree[attribute][data_attribute_value] = subtree
	#print stats_name
	#print attribute_error_stats[stats_name]['num_minus']
	#print attribute_error_stats[stats_name]['num_plus']
	#print node_error_estimate
	#print total_sub_tree_error_estimate
	#print "\n"
	if (node_error_estimate < total_sub_tree_error_estimate):
	#	print sub_tree_error
		print "Prunning Node!!!"
		my_new_classification = "+"
		if attribute_error_stats[stats_name]['num_minus'] > attribute_error_stats[stats_name]['num_plus']:
			my_new_classification = "-"
		return my_new_classification, node_error_estimate, node_N 
	return decision_tree, total_sub_tree_error_estimate, node_N 

def perform_classification(decision_tree, infilepath, outfilepath):
	in_fh = open(infilepath)
	out_fh = open(outfilepath, 'w')
	data = [row.strip().split(',') for row in in_fh.readlines()]
	for i in range(0, len(data)):
		# fill in the missing values and discretize the continuous variables
		for j in range(0, 15):
			if data[i][j] == '?':
				data[i][j] = ranges_dict[j]['most_common']
			if j in continuous:
				if float(data[i][j]) < ranges_dict[j]['min']:
					data[i][j] = 0	
				elif float(data[i][j]) > ranges_dict[j]['max']:
					data[i][j] = 9
				else:
					data[i][j] = int((float(data[i][j]) - ranges_dict[j]['min'])/ranges_dict[j]['interval'])
		v_tree = decision_tree
		# iterate through the attribute and value levels of the decision tree until reaching the classification 
		while v_tree != '+' and v_tree != '-':
			attribute = v_tree.keys()[0] 
			a_tree = v_tree[attribute]
			data_attribute_value = data[i][attribute]
			if data_attribute_value in a_tree:
				v_tree = a_tree[data_attribute_value]
				#print v_tree
			else: # default value
				v_tree = '-'
				break	
		data[i].append(v_tree)
		out_fh.writelines(["%s," % item  for item in data[i]])
		out_fh.write("\n")
		
if __name__ == '__main__':
	if len(sys.argv) < 5:
		print 'requires the following 4 file name arguments:  training_file, test_file, validation_file, classification_output_file'
		exit(-1)
	training_data = parse_training_set(sys.argv[1])
	attributes = set(category_dict.keys())
	print "calling generate decision tree with training file"
	decision_tree = generate_decision_tree(training_data, attributes, "TOP")
	#print "this is the initial un-pruned decision tree"
	#print decision_tree
	print "calling pruning algorithm with validation file"
	pruned_decision_tree = perform_pruning(decision_tree, sys.argv[2])
	#print attribute_error_stats
	#print "this is the final, pruned decision tree"
	#print pruned_decision_tree
	print "calling classification"
	perform_classification(pruned_decision_tree, sys.argv[3], sys.argv[4])	
