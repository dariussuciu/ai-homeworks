#i##########################################
# you need to implement five funcitons here
###########################################
from copy import deepcopy
from random import randrange
from random import choice
from sets import Set
# global variables
N,M,K = 0,0,0
FINAL_BOARD = []
DOMAINS = []
CONSISTENCY_CHECKS = 0

def valid_for_backtracking_constraints(value, row, col, board):
    global CONSISTENCY_CHECKS
    CONSISTENCY_CHECKS += 1
    # first check that this value is not already in this row
    if value in board[row]:
        return False
    # check that this value is not already in this column
    for r in range(0,N):
        if value == board[r][col]:
            return False
    # check that this value is not already inside the subgrid
    # if this is not the first row of a subgrid, we need to start earlier
    start_row = row - (row % M)
    stop_row = start_row + M
    # if this is not the first col of a subgrid, we need to start earlier
    start_col = col - (col % K)
    stop_col = start_col + K
    # check the subgrid, exclusive intervals
    for r in range(start_row, stop_row):
        for c in range(start_col, stop_col):
            if value == board[r][c]:
                return False
    # passed all checks
    return True

# get the number conflicts for a value
# on a given cell
def get_conflits_number(value, row, col, board):
    global CONSISTENCY_CHECKS
    CONSISTENCY_CHECKS += 1
	# we initialize it to -1 because we will count the value itself while
	# checking if it`s in the row
    conflicts = -1
    # first check that this value is not already in this row
    for cellValue in board[row]:
		if cellValue == value:
			conflicts += 1

    # check that this value is not already in this column
    for r in range(0,N):
        if r != row and value == board[r][col]:
            conflicts += 1
    # check that this value is not already inside the subgrid
    # if this is not the first row of a subgrid, we need to start earlier
    start_row = row - (row % M)
    stop_row = start_row + M
    # if this is not the first col of a subgrid, we need to start earlier
    start_col = col - (col % K)
    stop_col = start_col + K
    # check the subgrid, exclusive intervals
    for r in range(start_row, stop_row):
        for c in range(start_col, stop_col):
            if (not (row == r and col ==c)) and value == board[r][c]:
                conflicts += 1
    # return the found conflicts
    return conflicts

#get the value that gives the least conflicts
def get_min_conflicts_value(row, col, board):
	#initialize to a big unreachable value
    min_conflicts = 1000
    min_conflict_value = board[row][col]
    for value in DOMAINS[row][col]:
		#get the number of conflicts for a possible assignment
        conflicts = get_conflits_number(value, row, col, board)
        if min_conflicts > conflicts:
            min_conflicts = conflicts
            min_conflict_value = value
    return min_conflict_value

def simple_recursive_backtracking(row, col, board):
    global FINAL_BOARD
    last_cell = False
    # see if we are about to fill in the last cell - this is where the search terminates
    # this game is guaranteed to have a solution - we will set the final board after assigning the last value
    if (row + 1 == N) and (col + 1 == N):
        #print "deciding that we are in the last cell"
        last_cell = True
    if board[row][col] == "-":
        # try all possible values for this cell
        for value in range(1,N+1):
            if valid_for_backtracking_constraints(value,row,col,board) == True:
                # set this value
                board[row][col] = value
                # first check if we are done
                if last_cell == True:
                     FINAL_BOARD = deepcopy(board)
                     print 'Solution Found'
                     return True
                else:
                    # assume success here, and move on to the next col, or the next row
                    next_col = col + 1
                    next_row = row
                    if next_col == N:
                        next_col = 0
                        next_row = row + 1
                    if simple_recursive_backtracking(next_row, next_col, board) == True:
                        return True
        # we tried all possbile values of the cell, and no success
        # so we are returning the value back to "-"
        board[row][col] = '-'
        return False
    else:  # be very carefull here, this code is basically duplicated from above
        # this must have been a pre-filled in value
        # first check if we are done
        if last_cell == True:
            FINAL_BOARD = deepcopy(board)
            return True
        else:
            # assume success here, and move on to the next col, or the next row
            next_col = col + 1
            next_row = row
            if next_col == N:
                next_col = 0
                next_row = row + 1
            if simple_recursive_backtracking(next_row, next_col, board) == True:
               return True # I think this has to be true, we can not modify this cell

def backtracking(filename):
    ###
    # use backtracking to solve sudoku puzzle here,
    # return the solution in the form of list of
    # list as describe in the PDF with # of consistency
    # checks done
    ###
    # read the file header
    global N,M,K
    fh = open(filename, 'r')
    N,M,K = fh.readline().strip().strip(';').split(',')
    N = int(N)
    M = int(M)
    K = int(K)
    # read in the board, which is a list of rows
    board = [row.strip().strip(';').split(',') for row in fh.readlines()]
    for row in range(0,N):
        for col in range(0,N):
            if board[row][col] != "-":
                board[row][col] = int(board[row][col])
    # call the recursive simple back tracking
    simple_recursive_backtracking(0,0,board)
    return (FINAL_BOARD, CONSISTENCY_CHECKS)

###########################################

# this function will set up the initial values for the domains of all cells
# removing from the domain those values that conflict with numbers assigned in
# the setup;
def setup_domains(board):
    global DOMAINS
    # hack to define the domains
    DOMAINS = deepcopy(board)
    # convert already specified values in the grid to integers
    for row in range(0,N):
        for col in range(0,N):
            if board[row][col] != "-":
                board[row][col] = int(board[row][col])
    for row in range(0,N):
        for col in range(0,N):
            DOMAINS[row][col] = set()
            # unassigned cell, check all the constraints to make sure its valid given the existing assigned numbers
            if board[row][col] == "-":
                for value in range(1,N+1):
                    if valid_for_backtracking_constraints(value,row,col,board) == True:
                        DOMAINS[row][col].add(value)
            else:
                DOMAINS[row][col].add(board[row][col])

# this function will set up the initial values for the domains of all cells
# and initialize the variables with random values
def setup__min_conflicts_domains_and_board(board):
    global DOMAINS
    global CONFLICTCELLS
    global INITIALBOARD
    CONFLICTCELLS = []
    # hack to define the domains
    DOMAINS = deepcopy(board)
    # save the initial board in order to determine which values we can change
    INITIALBOARD = deepcopy(board)
    # convert already specified values in the grid to integers
    for row in range(0,N):
        for col in range(0,N):
            if board[row][col] != "-":
                board[row][col] = int(board[row][col])
    for row in range(0,N):
        for col in range(0,N):
            DOMAINS[row][col] = set()

            if board[row][col] == "-":
                for value in range(1,N+1):
                        DOMAINS[row][col].add(value)
				# assign initially a random value
                cellValue = randrange(1,N+1, 1)
                board[row][col] = cellValue
				#initialize the list of conflicted cells
                conflictsNumber = get_conflits_number(cellValue, row, col, board)
                if conflictsNumber > 0:
                    CONFLICTCELLS.append((row, col))

# this function will update the conflicted cells list based
# on the current board
def update_conflict_cells(board):
    global CONFLICTCELLS
    CONFLICTCELLS = []
    for row in range(0,N):
        for col in range(0,N):
            # check if we are allowed to change the cell
            if INITIALBOARD[row][col] != "-":
                continue
            else:
                conflictsNumber = get_conflits_number(board[row][col], row, col, board)
                if conflictsNumber > 0:
                    CONFLICTCELLS.append((row, col))



# once we assign a value to a cell, we need to remove this value from the domains
# of all the cells in that row, col, and subgrid
# similarly, if we back-track, we need to restore the value back to the other cells
# will remove value if remove=True, otherwise will restore the value
# return True if we still have some possible values or False if we hit a dead end
def edit_domains(value, row, col, board, remove):
    global DOMAINS

    # remove or restore the value from the row DOMAINS, do not modify already assigned cells
    for c in range(0,N):
        if remove == True:
            if (board[row][c] == "-") and (value in DOMAINS[row][c]):
                DOMAINS[row][c].remove(value)
                if not DOMAINS[row][c]:
                    return False
        else: #restore
            if (board[row][c] == "-"):
                DOMAINS[row][c].add(value)
    # remove or restore the value from the col DOMAINS, do not modify already assigned cells
    for r in range(0,N):
        if remove == True:
            if (board[r][col] == "-") and (value in DOMAINS[r][col]):
                DOMAINS[r][col].remove(value)
                if not DOMAINS[r][col]:
                    return False
        else: #restore
            if (board[r][col] == "-"):
                DOMAINS[r][col].add(value)
    # remove or restore the value from the subgrid DOMAINS, do not modify already assigned cells
    start_row = row - (row % M)
    stop_row = start_row + M
    # if this is not the first col of a subgrid, we need to start earlier
    start_col = col - (col % K)
    stop_col = start_col + K
    # check the subgrid, exclusive intervals
    for r in range(start_row, stop_row):
        for c in range(start_col, stop_col):
            if remove == True:
                if (board[r][c] == "-") and (value in DOMAINS[r][c]):
                    DOMAINS[r][c].remove(value)
                    if not DOMAINS[r][c]:
                        return False
            else: #restore
                if (board[r][c] == "-"):
                    DOMAINS[r][c].add(value)
    return True

# this performs arc propagation between a cell
# and all its neighbours
def recursive_arc_propagation(row, col, board):
    global DOMAINS
    if not DOMAINS[row][col]:
        return False
	# we are changing the domains of the iterated variable
	# so we need a temporary duplicate list
    TempDomains = deepcopy(DOMAINS)
    needToCheckNeighbours = False
    for value in TempDomains[row][col]:
        # remove or restore the value from the row DOMAINS, do not modify predefined assigned cells
        for c in range(0,N):
            if c == col:
                continue
            if value in DOMAINS[row][c]:
				# remove the value to test if we have alternatives
                DOMAINS[row][c].remove(value)
                if not DOMAINS[row][c]:
					# add the value back
                    DOMAINS[row][c].add(value)
					# make the arc consistent
                    DOMAINS[row][col].remove(value)
                    needToCheckNeighbours = True
                    break
                else:
					# add the value back
                    DOMAINS[row][c].add(value)

		# we removed the value, so we go to the next one
        if needToCheckNeighbours:
                continue

        # remove or restore the value from the col DOMAINS, do not modify predefined assigned cells
        for r in range(0,N):
            if r == row:
                continue

            if value in DOMAINS[r][col]:
				# remove the value to test if we have alternatives
                DOMAINS[r][col].remove(value)
                if not DOMAINS[r][col]:
					# add the value back
                    DOMAINS[r][col].add(value)
					# make the arc consistent
                    DOMAINS[row][col].remove(value)
                    needToCheckNeighbours = True
                    break
                else:
					# add the value back
                    DOMAINS[r][col].add(value)

		# we removed the value, so we go to the next one
        if needToCheckNeighbours:
            continue

        # remove or restore the value from the subgrid DOMAINS, do not modify already assigned cells
        start_row = row - (row % M)
        stop_row = start_row + M
        # if this is not the first col of a subgrid, we need to start earlier
        start_col = col - (col % K)
        stop_col = start_col + K
        # check the subgrid, exclusive intervals
        for r in range(start_row, stop_row):
            for c in range(start_col, stop_col):
                if c == col and r == row:
                    continue
                if value in DOMAINS[r][c]:
					# remove the value to test if we have alternatives
                    DOMAINS[r][c].remove(value)
                    if not DOMAINS[r][c]:
						# add the value back
                        DOMAINS[r][c].add(value)
						# make the arc consistent
                        DOMAINS[row][col].remove(value)
                        needToCheckNeighbours = True
                        break
                    else:
						# add the value back
                        DOMAINS[r][c].add(value)

	# if there are values removed from any domains
	# we need to perform arc propagation from that cell
    if needToCheckNeighbours:
        for c in range(0,N):
            if board[row][c] == '-' and value in DOMAINS[row][c]:
                if not recursive_arc_propagation(row, c, board):
                    return False
        for r in range(0,N):
            if board[r][col] == '-' and value in DOMAINS[r][col]:
                if not recursive_arc_propagation(r, col, board):
                    return False

        # remove or restore the value from the subgrid DOMAINS, do not modify already assigned cells
        start_row = row - (row % M)
        stop_row = start_row + M
        # if this is not the first col of a subgrid, we need to start earlier
        start_col = col - (col % K)
        stop_col = start_col + K
        # check the subgrid, exclusive intervals
        for r in range(start_row, stop_row):
            for c in range(start_col, stop_col):
                if board[r][c] == "-" and value in DOMAINS[r][c]:
                     if not recursive_arc_propagation(r, c, board):
                         return False
    return True

# returns one of the cells with the minimum number of remaining choices, returns row,col
# if returns -1,-1, all the cells have been assigned, and we are done
def MRV_heuristic_and_final_check(board):
    min_choices = 10000
    min_row = -1
    min_col = -1



    for r in range(0,N):
        for c in range(0,N):
            if (board[r][c] == "-") and (len(DOMAINS[r][c]) < min_choices):
                min_choices = len(DOMAINS[r][c])
                min_row = r
                min_col = c
    return min_row,min_col

# check to see if the neighbours also have the value in their domains
# retrieve the value that would remove the least amount of values from
# neighboring domains
def LCV_heuristic(row, col, board, domain):
    min_existing_values = 9999
    best_value = 0
    for value in domain:
        existing_value = 0
        for c in range(0,N):
            if value in DOMAINS[row][c]:
                existing_value += 1
        for r in range(0,N):
            if value in DOMAINS[r][col]:
                existing_value += 1

        # remove or restore the value from the subgrid DOMAINS, do not modify already assigned cells
        start_row = row - (row % M)
        stop_row = start_row + M
        # if this is not the first col of a subgrid, we need to start earlier
        start_col = col - (col % K)
        stop_col = start_col + K
        # check the subgrid, exclusive intervals
        for r in range(start_row, stop_row):
            for c in range(start_col, stop_col):
                if value in DOMAINS[r][c]:
                    existing_value += 1

        if min_existing_values > existing_value:
            min_existing_values = existing_value
            best_value = value

    return best_value

def recursive_backtrackingMRV(board, useforwardChecking, useCP):
    global FINAL_BOARD

    if useCP:
        for r in range(0,N):
            for c in range(0,N):
                if board[r][c] == '-':
                    recursive_arc_propagation(r, c, board)

    # get the most constrained unassigned cell
    row,col = MRV_heuristic_and_final_check(board)
    # we are done
    if (row == -1 and col==-1):
        FINAL_BOARD = deepcopy(board)
        print 'Solution Found'
        return True

    tempDomain = deepcopy(DOMAINS[row][col])
    #  iterate over all values in the DOMAIN using the LCV heuristic
    for i in range(0, len(DOMAINS[row][col])):

        value = LCV_heuristic(row, col, board, tempDomain)
        # check if we still have values in the domain
        if value == 0:
            break
        # remove the bad value, so we don`t try again
        tempDomain.remove(value)
        if not valid_for_backtracking_constraints(value,row,col,board):
            continue
        # set the value
        board[row][col] = value

        if useforwardChecking:
            # remove this value from all the conflicting domains
            if not edit_domains(value, row, col, board, True):
                edit_domains(value, row, col, board, False)
                continue

        if recursive_backtrackingMRV(board, useforwardChecking, useCP):
            return True
    # we tried all the possible values for this cell, and no success
    # return the value to 0
    # return all possibilities to the previously removed domains
    board[row][col] = "-"
    return False

def backtrackingMRV(filename):
    ###
    # use backtracking + MRV to solve sudoku puzzle here,
    # return the solution in the form of list of
    # list as describe in the PDF with # of consistency
    # checks done
    ###
    global N,M,K
    global CONSISTENCY_CHECKS
    fh = open(filename, 'r')
    N,M,K = fh.readline().strip().strip(';').split(',')
    N = int(N)
    M = int(M)
    K = int(K)
    # read in the board, which is a list of rows
    board = [row.strip().strip(';').split(',') for row in fh.readlines()]
    # fill in the valid domains
    setup_domains(board)
    # call the consistency checks to set them to 0 before the recursive backtracking
    CONSISTENCY_CHECKS = 0
    # call the recursive simple back tracking
    recursive_backtrackingMRV(board, False, False)
    return (FINAL_BOARD, CONSISTENCY_CHECKS)

def backtrackingMRVfwd(filename):
    ###
    # use backtracking + MRV to solve sudoku puzzle here,
    # return the solution in the form of list of
    # list as describe in the PDF with # of consistency
    # checks done
    ###
    global N,M,K
    global CONSISTENCY_CHECKS
    fh = open(filename, 'r')
    N,M,K = fh.readline().strip().strip(';').split(',')
    N = int(N)
    M = int(M)
    K = int(K)
    # read in the board, which is a list of rows
    board = [row.strip().strip(';').split(',') for row in fh.readlines()]
    # fill in the valid domains
    setup_domains(board)
    # call the consistency checks to set them to 0 before the recursive backtracking
    CONSISTENCY_CHECKS = 0
    # call the recursive simple back tracking
    recursive_backtrackingMRV(board, True, False)
    return (FINAL_BOARD, CONSISTENCY_CHECKS)

def backtrackingMRVcp(filename):
    ###
    # use backtracking + MRV to solve sudoku puzzle here,
    # return the solution in the form of list of
    # list as describe in the PDF with # of consistency
    # checks done
    ###
    global N,M,K
    global CONSISTENCY_CHECKS
    fh = open(filename, 'r')
    N,M,K = fh.readline().strip().strip(';').split(',')
    N = int(N)
    M = int(M)
    K = int(K)
    # read in the board, which is a list of rows
    board = [row.strip().strip(';').split(',') for row in fh.readlines()]
    # fill in the valid domains
    setup_domains(board)
    # call the consistency checks to set them to 0 before the recursive backtracking
    CONSISTENCY_CHECKS = 0

    # call the recursive simple back tracking
    recursive_backtrackingMRV(board, False, True)
    return (FINAL_BOARD, CONSISTENCY_CHECKS)

def minConflict(filename):
    ###
    # use minConflict to solve sudoku puzzle here,
    # return the solution in the form of list of
    # list as describe in the PDF with # of consistency
    # checks done
    ###
    global N,M,K
    global CONSISTENCY_CHECKS
    fh = open(filename, 'r')
    N,M,K = fh.readline().strip().strip(';').split(',')
    N = int(N)
    M = int(M)
    K = int(K)
    # read in the board, which is a list of rows
    board = [row.strip().strip(';').split(',') for row in fh.readlines()]
    # fill in the valid domains
    setup__min_conflicts_domains_and_board(board)
    # call the consistency checks to set them to 0 before the recursive backtracking
    CONSISTENCY_CHECKS = 0
    while CONFLICTCELLS:
		#chose a random conflicted cell
        randomCell = choice(CONFLICTCELLS)
        row = randomCell[0]
        col = randomCell[1]
		#get the best value for this cell
        value = get_min_conflicts_value(row, col, board)
        board[row][col] = value
		#get the new list of conflicted cells
        update_conflict_cells(board)
    FINAL_BOARD = deepcopy(board)
    return (FINAL_BOARD, CONSISTENCY_CHECKS)
