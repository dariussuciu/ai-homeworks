import sys
# global variables
rows,columns = 0,0


def parse_file(filepath):
	# read the layout file to the board array
	board = []
	fh = open(filepath)
	global rows,columns
	rows,columns = fh.readline().split(' ')
	rows = int(rows)
	columns = int(columns)
	board = [row.strip().split(',') for row in fh.readlines()]
	fh.close()
	return board

def getNeighborVariableName(row, col):
	return (row * columns) + col + 1

def recursiveCNFConstruction(mines,neighbors):
	andClauses = []
	#print "recursiveCNF"
	#print mines,len(neighbors)
	# define the base cases
	# mines = 0 -> all the neighbors must be false
	if mines == 0:
		for n in range(0,len(neighbors)):
			andClauses.append([-1*neighbors[n]])	
		return andClauses
	# mines = # of neighbors -> all neighbors must be true 
	if mines == len(neighbors):
		for n in range(0,len(neighbors)):
			andClauses.append([neighbors[n]])	
		return andClauses
	# call the function recursively by removing one neighbor, and considering
	# cases where it is true or false
	n1 = neighbors[0]

	# assume n1 is true, then it implies there are mines-1 in rest of neighbors
	andClauses = recursiveCNFConstruction(mines-1,neighbors[1:])
	#print "got andClauses from child"
	#print andClauses
	# now go through all the clauses, and OR them with -1*n1 to get the implication	
	for i in range(0,len(andClauses)):
		andClauses[i].append(-1*n1)
	#print "and Clauses after processing"
	#print andClauses

	# now the second case, assume n1 is false, it implies that there are n mines in the rest
	otherClauses = recursiveCNFConstruction(mines,neighbors[1:])
	#print "got otherClauses from child"
	#print otherClauses
	# now go through all the clauses, and OR them with n1 to get the implication, and add
	for i in range(0,len(otherClauses)):
		new_clause = otherClauses[i]
		new_clause.append(n1)
		andClauses.append(new_clause)

	return andClauses

def printCNF(andClauses, fh):
	global rows,columns	
	firstline = "p cnf " + str(rows*columns) + " " + str(len(andClauses))
	fh.write(firstline)
	fh.write("\n")
	for i in range(0,len(andClauses)):
		andclause = ""
		for j in range(0,len(andClauses[i])):
			andclause = andclause + str(andClauses[i][j]) + " ";
		andclause = andclause + str(0)
		fh.write(andclause)
		fh.write("\n")
			
		 
def convert2CNF(board, output):
	fout = open(output, 'w')
	global rows,columns
	andClauses = [] 
	for row in range(0,rows):
		for col in range(0,columns):
			if (board[row][col] != 'X'):
				# get the number of mines that the neighbors have
				mines = int(board[row][col])
				# add a constraint that this cell does NOT have a mine!!!!!!!!!!!!!!
				andClauses.append([-1*getNeighborVariableName(row,col)])
				# get the list of all neighbors
				neighbors = []	
				if col > 0:
					neighbors.append(getNeighborVariableName(row,col-1))
					if row > 0 :
						neighbors.append(getNeighborVariableName(row-1,col-1))
					if row < (rows -1):
						neighbors.append(getNeighborVariableName(row+1,col-1))
				if col < (columns - 1):
					neighbors.append(getNeighborVariableName(row,col+1))
					if row > 0:
						neighbors.append(getNeighborVariableName(row-1,col+1))
					if row < (rows -1):
						neighbors.append(getNeighborVariableName(row+1,col+1))
				if row > 0:
					neighbors.append(getNeighborVariableName(row-1,col))
				if row < (rows -1):
					neighbors.append(getNeighborVariableName(row+1,col))
				#print neighbors
				# now construct all the cnf clauses for the neighbors of this variable
				andClauses.extend(recursiveCNFConstruction(mines,neighbors))
	printCNF(andClauses,fout)
	fout.close()

if __name__ == '__main__':
	if len(sys.argv) < 3:
		print 'Layout or output file not specified.'
		exit(-1)
	board = parse_file(sys.argv[1])
	convert2CNF(board, sys.argv[2])
