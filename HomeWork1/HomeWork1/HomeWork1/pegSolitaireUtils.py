from copy import deepcopy
import readGame
import config

#######################################################
# These are some Helper functions which you have to use 
# and edit.
# Must try to find out usage of them, they can reduce
# your work by great deal.
#
# Functions to change:
# 1. is_wall(self, pos):
# 2. is_validMove(self, oldPos, direction):
# 3. getNextPosition(self, oldPos, direction):
# 4. getNextState(self, oldPos, direction):
#######################################################
class game:
	def __init__(self, filePath):
		self.gameState = readGame.readGameState(filePath)
		self.nodesExpanded = 0
		self.trace = []

	def is_corner(self, pos):
		if self.gameState[pos[0]][pos[1]] == -1:
			return True
		return False

	def is_occupied(self, pos):
		if self.gameState[pos[0]][pos[1]] == 1:
			return True
		return False

	def is_outside(self, pos):
		if self.gameState[pos[0]][pos[1]] in [-1, 0, 1]:
			return False
		return False

	def is_correct_jump(self, oldPos, direction):
		intPos = self.getIntPosition(oldPos,direction)

		if self.gameState[intPos[0]][intPos[1]] != 1:
			return False
		else:
			return True

	def getIntPosition(self, oldPos, direction):
		newPos = deepcopy(oldPos)
		newPos[0] += direction[0]
		newPos[1] += direction[1]
		return newPos

	def getNextPosition(self, oldPos, direction):
		#########################################
		# YOU HAVE TO MAKE CHANGES HERE
		# See DIRECTION dictionary in config.py and add
		# this to oldPos to get new position of the peg if moved
		# in given direction , you can remove next line
		newPos = deepcopy(oldPos)
		newPos[0] += direction[0] * 2
		newPos[1] += direction[1] * 2
		return newPos

	def getPrevPosition(self, oldPos, direction):
		#########################################
		# YOU HAVE TO MAKE CHANGES HERE
		# See DIRECTION dictionary in config.py and add
		# this to oldPos to get new position of the peg if moved
		# in given direction , you can remove next line
		newPos = deepcopy(oldPos)
		newPos[0] -= direction[0] * 2
		newPos[1] -= direction[1] * 2
		return newPos

	def is_validMove(self, oldPos, direction):

		rows = len(self.gameState)
		cols = len(self.gameState[0])


		#########################################
		# DONT change Things in here
		# In this we have got the next peg position and
		# below lines check for if the new move is a corner
		newPos = self.getNextPosition(oldPos, direction)

		if newPos[0] < 0:
			return False

		if newPos[0] >= rows:
			return False

		if newPos[1] < 0:
			return False

		if newPos[1] >= cols:
			return False

		if self.is_corner(newPos):
			return False
		#########################################

		if self.is_occupied(newPos):
			return False

		if self.is_outside(newPos):
			return False

		if not self.is_correct_jump(oldPos, direction):
			return False

		########################################
		# YOU HAVE TO MAKE CHANGES BELOW THIS
		# check for cases like:
		# if new move is already occupied
		# or new move is outside peg Board
		# Remove next line according to your convenience

		return True

	def getNextState(self, oldPos, directionString):
		direction = self.getDirectionValue(directionString)
		###############################################
		# DONT Change Things in here
		self.nodesExpanded += 1
		if not self.is_validMove(oldPos, direction):
			print "Error, You are not checking for valid move"
			exit(0)
		###############################################

		self.setPegBoardValue(oldPos, 0)
		newPos = self.getNextPosition(oldPos, direction)
		self.setPegBoardValue(newPos, 1)
		intPos = self.getIntPosition(oldPos, direction)
		self.setPegBoardValue(intPos, 0)

		###############################################
		# YOU HAVE TO MAKE CHANGES BELOW THIS
		# Update the gameState after moving peg
		# eg: remove crossed over pegs by replacing it's
		# position in gameState by 0
		# and updating new peg position as 1

		return self.gameState

	def getPrevState(self, oldPos, directionString):
		direction = self.getDirectionValue(directionString)

		self.setPegBoardValue(oldPos, 1)
		newPos = self.getNextPosition(oldPos, direction)
		self.setPegBoardValue(newPos, 0)
		intPos = self.getIntPosition(oldPos, direction)
		self.setPegBoardValue(intPos, 1)

		return self.gameState

	def getDirectionValue(self, directionString):
		if directionString == 'N':
			return config.DIRECTION.get('N')
		elif directionString == 'S':
			return config.DIRECTION.get('S')
		elif directionString == 'W':
			return config.DIRECTION.get('W')
		elif directionString == 'E':
			return config.DIRECTION.get('E')

	def setPegBoardValue(self, pos, value):
		self.gameState[pos[0]][pos[1]] = value

	def isFinished(self):
		# very simple function: checks if the sum is 15
		# which is equivalent to 1 peg and 16 corners
		# and if the single peg is in the center
		mysum = sum(sum(x) for x in self.gameState)
		if mysum != -15:
			return False
		if self.gameState[3][3] != 1:
			return False
		return True
