from copy import deepcopy
import pegSolitaireUtils
import config
import Queue as Q

def ItrDeepSearch(pegSolitaireObject):

	#################################################
	# Must use functions:
	# getNextState(self,oldPos, direction)
	#
	# we are using this function to count,
	# number of nodes expanded, If you'll not
	# use this grading will automatically turned to 0
	#################################################
	#
	# using other utility functions from pegSolitaireUtility.py
	# is not necessary but they can reduce your work if you
	# use them.
	# In this function you'll start from initial gameState
	# and will keep searching and expanding tree until you
	# reach goal using Iterative Deepning Search.
	# you must save the trace of the execution in pegSolitaireObject.trace
	# SEE example in the PDF to see what to save
	#
	#################################################

	maxDepth = 34
	while not depthLimitedSearch(pegSolitaireObject, maxDepth):
		maxDepth += 1
		print maxDepth
	return True

def depthLimitedSearch(pegSolitaireObject, maxDepth):
	stack = []
	depth = 0
	if getSuccessors(stack, None, None, pegSolitaireObject, None, depth + 1):
		while stack:
			currentNode = deepcopy(stack[len(stack) - 1])
			oldPos = currentNode[0]
			direction = currentNode[1]
			visited = currentNode[2]
			currentDepth = currentNode[3]
			if visited:
				pegSolitaireUtils.game.getPrevState(pegSolitaireObject, oldPos, direction)
				pegSolitaireObject.trace.pop()
				stack.pop()
				continue
			pegSolitaireUtils.game.getNextState(pegSolitaireObject, oldPos, direction)
			if pegSolitaireObject.isFinished():
				nextPos = pegSolitaireUtils.game.getNextPosition(pegSolitaireObject, oldPos, config.DIRECTION.get(direction))
				pegSolitaireObject.trace.append((oldPos, nextPos))
				return True
				break
			elif currentDepth >= maxDepth:
				pegSolitaireUtils.game.getPrevState(pegSolitaireObject, oldPos, direction)
				stack.pop()
			else:
				if getSuccessors(stack, oldPos, direction, pegSolitaireObject, currentNode, depth + 1):
					depth += 1
				else:
					pegSolitaireUtils.game.getPrevState(pegSolitaireObject, oldPos, direction)
					stack.pop()
					pegSolitaireObject.trace.pop()
		return False

def getSuccessors(stack, oldPos, oldDirection, pegSolitaireObject, currentNode, depth):
	if currentNode:
		stack[len(stack) - 1] = (currentNode[0], currentNode[1], True, currentNode[3])
		nextPos = pegSolitaireUtils.game.getNextPosition(pegSolitaireObject, oldPos, config.DIRECTION.get(oldDirection))
		pegSolitaireObject.trace.append((oldPos, nextPos))
	elementAdded = False
	rows = len(pegSolitaireObject.gameState)
	cols = len(pegSolitaireObject.gameState[0])
	for i in range(rows):
		for j in range(cols):
			if pegSolitaireObject.gameState[i][j] == 1:
				pos = [i, j]
				successors = []
				for direction in config.DIRECTION.keys():
					if pegSolitaireUtils.game.is_validMove(pegSolitaireObject, pos, config.DIRECTION.get(direction)):
						successors.insert(0, (pos, direction, False, depth))
						elementAdded = True
				stack += successors
	if elementAdded:
		return True
	else:
		return False

def aStarOne(pegSolitaireObject):
	priorityQueue = Q.PriorityQueue()
	dictionary = {}
	dictionaryKey = 0
	if getA1Successors(priorityQueue, pegSolitaireObject, dictionary, dictionaryKey, None):
		while priorityQueue:
			nextEntry = priorityQueue.get()
			currentNode = deepcopy(nextEntry[1])
			oldPos = currentNode[0]
			direction = currentNode[1]
			dictionaryKey = currentNode[2]
			dictionaryPos = currentNode[3]
			pegSolitaireUtils.game.getNextState(pegSolitaireObject, oldPos, direction)
			if pegSolitaireObject.isFinished():
				return True
				break
			if not getA1Successors(priorityQueue, pegSolitaireObject, dictionary, dictionaryKey, dictionaryPos):
				pegSolitaireUtils.game.getPrevState(pegSolitaireObject, oldPos, direction)
		return False

	#################################################
        # Must use functions:
        # getNextState(self,oldPos, direction)
        # 
        # we are using this function to count,
        # number of nodes expanded, If you'll not
        # use this grading will automatically turned to 0
        #################################################
        #
        # using other utility functions from pegSolitaireUtility.py
        # is not necessary but they can reduce your work if you 
        # use them.
        # In this function you'll start from initial gameState
        # and will keep searching and expanding tree until you 
	# reach goal using A-Star searching with first Heuristic
	# you used.
        # you must save the trace of the execution in pegSolitaireObject.trace
        # SEE example in the PDF to see what to return
        #
        #################################################
	return True



def getA1Successors(queue, pegSolitaireObject, dictionary, dictionaryKey, parentDictionaryPos):
	elementAdded = False
	rows = len(pegSolitaireObject.gameState)
	cols = len(pegSolitaireObject.gameState[0])
	for i in range(rows):
		for j in range(cols):
			if pegSolitaireObject.gameState[i][j] == 1:
				pos = [i, j]
				for direction in config.DIRECTION.keys():
					if pegSolitaireUtils.game.is_validMove(pegSolitaireObject, pos, config.DIRECTION.get(direction)):
						dictionaryKey += 1
						queue.put((1, (pos, direction, dictionaryKey, parentDictionaryPos)))
						dictionary[dictionaryKey] = (pos, parentDictionaryPos)
						elementAdded = True
	if elementAdded:
		return True
	else:
		return False

def aStarTwo(pegSolitaireObject):
	#################################################
        # Must use functions:
        # getNextState(self,oldPos, direction)
        # 
        # we are using this function to count,
        # number of nodes expanded, If you'll not
        # use this grading will automatically turned to 0
        #################################################
        #
        # using other utility functions from pegSolitaireUtility.py
        # is not necessary but they can reduce your work if you 
        # use them.
        # In this function you'll start from initial gameState
        # and will keep searching and expanding tree until you 
        # reach goal using A-Star searching with second Heuristic
        # you used.
        # you must save the trace of the execution in pegSolitaireObject.trace
        # SEE example in the PDF to see what to return
        #
        #################################################
	return True
