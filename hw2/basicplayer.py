from util import memoize, run_search_function
from util import INFINITY, NEG_INFINITY
import connectfour

def basic_evaluate(board):
    """
    The original focused-evaluate function from the lab.
    The original is kept because the lab expects the code in the lab to be modified. 
    """
    if board.is_game_over():
        # If the game has been won, we know that it must have been
        # won or ended by the previous move.
        # The previous move was made by our opponent.
        # Therefore, we can't have won, so return -1000.
        # (note that this causes a tie to be treated like a loss)
        score = -1000
    else:
        score = board.longest_chain(board.get_current_player_id()) * 10
        # Prefer having your pieces in the center of the board.
        for row in range(6):
            for col in range(7):
                if board.get_cell(row, col) == board.get_current_player_id():
                    score -= abs(3-col)
                elif board.get_cell(row, col) == board.get_other_player_id():
                    score += abs(3-col)

    return score


def get_all_next_moves(board):
    """ Return a generator of all moves that the current player could take from this position """
    from connectfour import InvalidMoveException

    for i in xrange(board.board_width):
        try:
            yield (i, board.do_move(i))
        except InvalidMoveException:
            pass

def is_terminal(depth, board):
    """
    Generic terminal state check, true when maximum depth is reached or
    the game has ended.
    """
    return depth <= 0 or board.is_game_over()

def negamax_value(board,depth,sign,eval_fn = basic_evaluate,
            get_next_moves_fn = get_all_next_moves,
            is_terminal_fn = is_terminal,
            verbose = True):
    # expanding the node, because this is where we are examining all the chilren
    if board.get_current_player_id() == 1:
        connectfour.increment_nodes_expanded()
    # using parameter sign to flip the sign between max and min turns
    # print "depth is %s " % depth
    max_col = -1
    max_val = NEG_INFINITY 
    if is_terminal_fn(depth, board):
        # this is where we call the function to evaluate the state at terminal depth or if game is over
        # print "reached terminal state, eval is %s" % eval_fn(board)
        return max_col, eval_fn(board)
    for move, new_board in get_next_moves_fn(board):
        # print "calling a child on %s" % move
        column, value = negamax_value(new_board,depth-1,-1*sign,eval_fn,get_next_moves_fn,is_terminal_fn,verbose)
        # this is negamax, so we must negate the value
        value *= -1 
        # print "got column %s, and value %s from child" % (column, value)
        # take the max
        if (max_val < value): 
            max_val = value
            # make sure we set the best column to the current move, not what our child returned
            max_col = move
            # print "now max column is  %s, and max value %s" % (max_col, max_val)
    # print "returning column %s, and value %s" % (max_col, max_val)
    return max_col, max_val

def minimax(board, depth, eval_fn = basic_evaluate,
            get_next_moves_fn = get_all_next_moves,
            is_terminal_fn = is_terminal,
            verbose = True):
    """
    Do a minimax search to the specified depth on the specified board.

    board -- the ConnectFourBoard instance to evaluate
    depth -- the depth of the search tree (measured in maximum distance from a leaf to the root)
    eval_fn -- (optional) the evaluation function to use to give a value to a leaf of the tree; see "focused_evaluate" in the lab for an example

    Returns an integer, the column number of the column that the search determines you should add a token to
    """
    # implementing the negamax as mentioned in the piazza comments
    # we will return the column and the value with our recursive negamax
    # this way we will know which move to make as we propagate up to the root
    column, value = negamax_value(board,depth,1,eval_fn,get_next_moves_fn,is_terminal_fn,verbose)
    return column


def rand_select(board):
    """
    Pick a column by random
    """
    import random
    moves = [move for move, new_board in get_all_next_moves(board)]
    return moves[random.randint(0, len(moves) - 1)]

def new_evaluate(board):
    if board.is_game_over():
        score = -1000000
    elif board.is_win():
        score = 1000000
    else:
        score = 0
        # calculate the score as the difference in the sum of the maximum chain
        # lengths chain containing each token in the board (prefer longer chains by using a weight)
        # between the current player and his opponent. To break ties we use the advantage of the
        # middle board positions (this is very useful in picking better positions in the beginning of the game)
        for row in range(6):
            for col in range(7):
                    if board.get_cell(row, col) == board.get_current_player_id():
                        score -= abs(3-col) - board.sum_of_length_from_cell(row, col)
                    elif board.get_cell(row, col) == board.get_other_player_id():
                        score -= board.sum_of_length_from_cell(row, col)
    return score

# an alternative (worse) heuristic for the 20 tokens version of the problem
# def new_evaluate(board):
#     if board.is_game_over():
#         score = -1000000
#     elif board.is_win():
#         score = 1000000
#     else:
#         score = 0
#         selfMax = 0
#         enemyMax = 0
#
#         for row in xrange(6):
#             for col in xrange(7):
#                 cell_player = board.get_cell(row, col)
#
#                 if cell_player == board.get_current_player_id():
#                     score -= abs(4-col)
#                     self_longest_chain = board.longest_chain(board.get_current_player_id())
#                     if selfMax < self_longest_chain:
#                         selfMax = self_longest_chain
#                 elif cell_player == board.get_other_player_id():
#                     score += abs(4-col)
#                     other_longest_chain = board.longest_chain(board.get_other_player_id())
#                     if enemyMax < other_longest_chain:
#                         enemyMax = other_longest_chain
#
#         score += (selfMax - enemyMax) * 100
#     return score

random_player = lambda board: rand_select(board)
basic_player = lambda board: minimax(board, depth=4, eval_fn=basic_evaluate)
new_player = lambda board: minimax(board, depth=4, eval_fn=new_evaluate)
progressive_deepening_player = lambda board: run_search_function(board, search_fn=minimax, eval_fn=basic_evaluate)
